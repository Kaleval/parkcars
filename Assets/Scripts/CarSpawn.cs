﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawn : MonoBehaviour
{
    [Header("Prefabs")]
    public GameObject prefCar;
    [Header("Parts")]
    public List<Renderer> colorThese;
    [Header("Debug")]
    public int id;
    public bool isUsed = false;

    public Car car;


    public void Init(int index)
    {
        id = index;
        ColorParts();
        //SpawnCar();
    }

    public void SpawnCar(Vector3[] waypoints)
    {
        CleanUp();
        car = Instantiate(prefCar, transform).GetComponent<Car>();
        car.Init(id, waypoints);

    }

    public void CleanUp()
    {
        foreach (CarDestination dest in FindObjectsOfType<CarDestination>())
        {
            dest.hasCar = false;
        }
        if (car != null)
        {
            Destroy(car.gameObject);
        }
    }

    void ColorParts()
    {
        foreach (Renderer r in colorThese)
        {
            r.material.color = GameManager.instance.activeLevel.GetColor(id);
        }
    }
}
