﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarDestination : MonoBehaviour
{
    [Header("Parts")]
    public List<Renderer> colorThese;
    [Header("Debug")]
    public int id;
    public bool isUsed = false;
    public bool hasCar = false;
    public void Init(int index)
    {
        id = index;
        ColorParts();
    }
    void ColorParts()
    {
        foreach (Renderer r in colorThese)
        {
            r.material.color = GameManager.instance.activeLevel.GetColor(id);
        }
    }
}
