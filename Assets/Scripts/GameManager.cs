﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class GameManager : MonoBehaviour
{
    [HideInInspector]
    public static GameManager instance;
    [Header("Prefabs")]
    public GameObject prefLevel;
    [Header("Debug")]
    public LevelController activeLevel;

    int currentLevel = 0;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        SpawnLevel(currentLevel);

    }

    public void LevelDone()
    {
        GetComponent<DrawManager>().CleanUp();
        activeLevel.MoveLevel(new Vector3(0, 0, -40), activeLevel.DestroyLevel);
        UIManager.instance.ShowMessage("Nice!");

        currentLevel++;
        SpawnLevel(currentLevel, new Vector3(0, 0, 40));
        activeLevel.MoveLevel(new Vector3(), StartLevel);
    }


    public void StartLevel()
    {
        UIManager.instance.ShowMessage("Level " + (currentLevel+1).ToString());
    }

    void SpawnLevel(int levelIndex, Vector3 position = new Vector3())
    {
        GameObject newLevel = Instantiate(prefLevel, position, transform.rotation);
        activeLevel = newLevel.GetComponent<LevelController>();
        activeLevel.InitLevel(levelIndex);
    }
}
