﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawManager : MonoBehaviour
{
    public GameObject prefLine;
    bool isDrawing = false;
    Line activeLine;

    List<Line> lines;
    // Start is called before the first frame update
    void Start()
    {
        lines = new List<Line>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // start draw
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            int layerMask = LayerMask.GetMask("Spawn");
            if (Physics.Raycast(ray, out RaycastHit hit, float.PositiveInfinity, layerMask))
            {
                CarSpawn spawn = hit.collider.GetComponent<CarSpawn>();
                if (spawn.isUsed == true)
                {
                    for (int i = 0; i < lines.Count; i++)
                    {
                        Line l = lines[i];
                        if (l.carSpawn == spawn)
                        {
                            lines.Remove(l);
                            l.Destroy();
                            break;
                        }
                    }
                }
                activeLine = Instantiate(prefLine, transform.position, transform.rotation, transform).GetComponent<Line>();
                lines.Add(activeLine);
                activeLine.Init(spawn.id);
                activeLine.SetSpawn(spawn);
                Vector3 newPos = hit.collider.transform.position;
                newPos.y = 0.1f;

                activeLine.AddWaypoint(newPos);
                isDrawing = true;

            }

        }
        else if (Input.GetMouseButton(0))
        {
            if (isDrawing && activeLine != null)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                int layerMask = LayerMask.GetMask("Destination", "Ground", "Spawn");
                if (Physics.Raycast(ray, out RaycastHit hit, layerMask))
                {
                    if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Spawn"))
                    {
                        //do nothinf :/
                        CarSpawn spawn = hit.collider.GetComponent<CarSpawn>();
                        if (spawn.id == activeLine.id)
                        {
                            return;
                        }
                    }
                    else if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Destination"))
                    {
                        CarDestination destination = hit.collider.GetComponent<CarDestination>();
                        if (destination.id == activeLine.id && destination.isUsed == false)
                        {
                            Vector3 newPos = hit.collider.transform.position;
                            newPos.y = 0.1f;
                            activeLine.SetDestination(destination);
                            activeLine.AddWaypoint(newPos);
                            LineDrawn();
                        }
                    }
                }
                else
                {
                    LineDrawn();
                }

                // draw
                if (isDrawing)
                {
                    Vector3 lastwaypoint = activeLine.GetLastWaypoint();
                    if (Vector3.Distance(lastwaypoint, hit.point) > 1f || float.IsNaN(lastwaypoint.x))
                    {
                        Vector3 newPos = hit.point;
                        newPos.y = 0.1f;
                        activeLine.AddWaypoint(newPos);
                    }
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            // end draw
            if (isDrawing)
            {
                LineDrawn();
            }
        }
    }
    public void CleanUp()
    {
        for (int i = lines.Count - 1; i >= 0; i--)
        {
            Line l = lines[i];
            lines.RemoveAt(i);
            Destroy(l.gameObject);
        }
    }
    public void LineDrawn()
    {
        isDrawing = false;
        if (activeLine != null)
        {
            activeLine = null;
        }
        foreach (Line line in lines)
        {
            line.SpawnCar();
        }
    }
}
