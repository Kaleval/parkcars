﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Line : MonoBehaviour
{
    public new LineRenderer renderer;
    public CarSpawn carSpawn;
    public CarDestination carDestination;
    public int id;

    Vector3[] waypoints;
    public void Init(int id)
    {
        this.id = id;
        renderer = GetComponent<LineRenderer>();
        renderer.startColor = GameManager.instance.activeLevel.GetColor(id);
        renderer.endColor = GameManager.instance.activeLevel.GetColor(id);
        waypoints = new Vector3[0];
    }

    public Vector3[] GetWaypoints()
    {
        Vector3[] pos = new Vector3[renderer.positionCount];
        renderer.GetPositions(pos);
        for (int i = 0; i < pos.Length; i++)
        {
            pos[i].y = 0;
        }
        return pos;
    }

    public Vector3 GetLastWaypoint()
    {
        if (waypoints.Length > 0)
        {
            Vector3 waypoint = waypoints[waypoints.Length - 1];
            return waypoint;
        }
        else
        {
            return new Vector3(float.NaN, float.NaN, float.NaN);
        }
    }

    public void AddWaypoint(Vector3 waypoint)
    {
        if (waypoints.Length > 0)
        {
            if (waypoint == waypoints[waypoints.Length - 1])
            {
                return;
            }
        }
        Vector3[] tempWaypoints = new Vector3[waypoints.Length + 1];
        waypoints.CopyTo(tempWaypoints, 0);
        waypoints = tempWaypoints;
        waypoints[waypoints.Length - 1] = waypoint;
        if (waypoints.Length > 2)
        {
            Vector3[] smooth = GenerateSmoothLine(waypoints);
            renderer.positionCount = smooth.Length;
            renderer.SetPositions(smooth);

        }
        else
        {
            renderer.positionCount = waypoints.Length;
            renderer.SetPositions(waypoints);
        }

    }

    public void SetSpawn(CarSpawn spawn)
    {
        carSpawn = spawn;
        spawn.isUsed = true;
    }

    public void SpawnCar()
    {
        carSpawn.SpawnCar(GetWaypoints());
    }

    public void SetDestination(CarDestination destination)
    {
        carDestination = destination;
        destination.isUsed = true;
    }

    public bool IsValidLine()
    {
        if (carSpawn != null && carDestination != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void Destroy()
    {
        if (carSpawn != null)
        {
            carSpawn.CleanUp();
            carSpawn.isUsed = false;
        }
        if (carDestination != null)
        {
            carDestination.isUsed = false;
        }
        Destroy(gameObject);
    }


    public Vector3[] GenerateSmoothLine(Vector3[] dataIn)
    {
        int pointCount = 3;
        Vector3[] dataOut = new Vector3[(dataIn.Length-1) * pointCount];
        int cntr = 0;
        // interpolate first rough "segment"
        for (int i = 0; i < pointCount; i++)
        {
            Vector3 p = Vector3.Lerp(dataIn[0], dataIn[1], (float)i / pointCount);
            dataOut[cntr++] = p;
        }
        // interpolate middle rough "segments"
        for (int i = 0; i <= dataIn.Length - 4; i++)
        {
            Vector3[] subarray = new Vector3[4];
            subarray[0] = dataIn[i];
            subarray[1] = dataIn[i + 1];
            subarray[2] = dataIn[i + 2];
            subarray[3] = dataIn[i + 3];
            foreach (Vector3 p in CatmulRom(subarray, pointCount))
            {
                dataOut[cntr++] = p;
            }
        }
        // interpolate last rough "segment"
        for (int i = 0; i < pointCount; i++)
        {
            Vector3 p = Vector3.Lerp(dataIn[dataIn.Length - 2], dataIn[dataIn.Length - 1], (float)i / pointCount);
            dataOut[cntr++] = p;

        }
        return dataOut;
    }


    Vector3[] CatmulRom(Vector3[] points, float amountOfPoints = 10.0f)
    {

        Vector2 p0 = new Vector2(points[0].x, points[0].z); // Vector3 has an implicit conversion to Vector2
        Vector2 p1 = new Vector2(points[1].x, points[1].z);
        Vector2 p2 = new Vector2(points[2].x, points[2].z);
        Vector2 p3 = new Vector2(points[3].x, points[3].z);

        float t0 = 0.0f;
        float t1 = GetT(t0, p0, p1);
        float t2 = GetT(t1, p1, p2);
        float t3 = GetT(t2, p2, p3);
        //int i = 0;

        Vector3[] newpoints = new Vector3[(int)(amountOfPoints)];
        for (int i = 0; i < amountOfPoints; i++)
        {
            float t = Mathf.Lerp(t1, t2, i / amountOfPoints);
            Vector2 A1 = (t1 - t) / (t1 - t0) * p0 + (t - t0) / (t1 - t0) * p1;
            Vector2 A2 = (t2 - t) / (t2 - t1) * p1 + (t - t1) / (t2 - t1) * p2;
            Vector2 A3 = (t3 - t) / (t3 - t2) * p2 + (t - t2) / (t3 - t2) * p3;

            Vector2 B1 = (t2 - t) / (t2 - t0) * A1 + (t - t0) / (t2 - t0) * A2;
            Vector2 B2 = (t3 - t) / (t3 - t1) * A2 + (t - t1) / (t3 - t1) * A3;

            Vector2 C = (t2 - t) / (t2 - t1) * B1 + (t - t1) / (t2 - t1) * B2;

            //Debug.Log(t1 + "/" + t + "/" + t2);
            //Debug.Log(i + "/" + amountOfPoints);
            newpoints[i] = new Vector3(C.x, 0.1f, C.y);
            if (float.IsNaN(C.x))
            {
                Debug.LogError("C.x is NaN for " + points[0] + " " + points[1] + " " + points[2] + " " + points[3]);
            }
        }

        return newpoints;
    }

    float GetT(float t, Vector2 p0, Vector2 p1)
    {
        float alpha = 0.5f;
        float a = Mathf.Pow((p1.x - p0.x), 2.0f) + Mathf.Pow((p1.y - p0.y), 2.0f);
        float b = Mathf.Pow(a, 0.5f);
        float c = Mathf.Pow(b, alpha);
        return (c + t);
    }
}
