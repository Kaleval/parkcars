﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
[RequireComponent(typeof(Rigidbody))]
public class Car : MonoBehaviour
{
    [Header("Parameters")]
    public float speed = 5;
    [Header("Parts")]
    public List<Renderer> colorThese;
    [Header("Debug")]
    public int id;


    new Rigidbody rigidbody;

    Vector3[] waypoints;
    bool isMoving = false;
    bool doneMoving = false;
    public int currentWaypoint = 0;


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Car"))
        {
            isMoving = false;
            rigidbody.isKinematic = false;
            ContactPoint contact = collision.contacts[0];
            rigidbody.AddForceAtPosition((contact.normal + Vector3.up).normalized * 100, contact.point);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (isMoving == false)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Destination") && other.GetComponent<CarDestination>().hasCar == false)
            {
                if (other.GetComponent<CarDestination>().id == id)
                {
                    other.GetComponent<CarDestination>().hasCar = true;
                    GameManager.instance.activeLevel.CheckDestinations();
                }
            }
        }
    }

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (isMoving)
        {
            if (currentWaypoint < waypoints.Length)
            {
                Vector3 target = waypoints[currentWaypoint];
                target.y = transform.position.y;
                Vector3 dir = target - transform.position;
                dir.Normalize();

                float angle = Vector3.SignedAngle(transform.forward, dir, Vector3.up);
                float value = Mathf.Min(Mathf.Abs(angle), 1000 * Time.deltaTime);
                transform.eulerAngles += new Vector3(0, Mathf.Sign(angle) * value, 0);

                Vector3 dist = dir * speed * Time.deltaTime;
                if (Vector3.Distance(transform.position, target) < dist.magnitude)
                {
                    transform.position += dir * speed * Time.deltaTime;
                    currentWaypoint++;
                    if (currentWaypoint < waypoints.Length - 1)
                    {
                        currentWaypoint++; // skipping every second waypoint
                    }
                }
                else
                {
                    transform.position += dir * speed * Time.deltaTime;
                }

            }
            else
            {
                rigidbody.velocity = new Vector3();// dir * speed;// * Time.deltaTime;
                isMoving = false;
            }
        }
    }

    public void Init(int index, Vector3[] waypoints)
    {
        id = index;
        ColorParts();
        this.waypoints = waypoints;
        StarMovement();
    }

    public void StarMovement()
    {
        currentWaypoint = 1;
        isMoving = true;
    }

    void ColorParts()
    {
        foreach (Renderer r in colorThese)
        {
            r.material.color = GameManager.instance.activeLevel.GetColor(id);
        }
    }
}
