﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[DisallowMultipleComponent]
public class UIManager : MonoBehaviour
{
    [HideInInspector]
    public static UIManager instance;

    public TextMeshProUGUI message;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ShowMessage(string text)
    {
        message.text = text;
        message.gameObject.SetActive(true);
        Invoke("HideMessage", 2.5f);
    }

    void HideMessage()
    {
        message.gameObject.SetActive(false);
    }


}
