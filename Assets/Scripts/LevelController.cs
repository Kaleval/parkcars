﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelController : MonoBehaviour
{
    [Header("Prefabs")]
    public GameObject prefCarSpawn;
    public GameObject prefCarDestination;

    [Header("Debug")]
    public List<CarSpawn> carSpawns;
    public List<CarDestination> carDestinations;

    List<Color> colors;

    bool moveLevel = false;
    Vector3 moveTo;
    UnityAction onDoneMovementAction;
    // Start is called before the first frame update
    void Awake()
    {
        carSpawns = new List<CarSpawn>();
        carDestinations = new List<CarDestination>();
        colors = new List<Color>();
    }


    private void Update()
    {
        if (moveLevel)
        {
            Vector3 dir = (moveTo - transform.position).normalized;
            float targetDist = Vector3.Distance(transform.position, moveTo);
            Vector3 step = dir * 30 * Time.deltaTime;
            float moveDist = step.magnitude;
            if (targetDist > moveDist)
            {
                transform.position += step;
            }
            else
            {
                transform.position = moveTo;
                moveLevel = false;
                onDoneMovementAction.Invoke();
            }
            
        }
    }


    public void DestroyLevel()
    {
        Destroy(gameObject);
    }
    public Color GetColor(int index)
    {
        return colors[index];
    }

    public void CheckDestinations()
    {
        bool levelCompleted = true;
        foreach(CarDestination dest in carDestinations)
        {
            levelCompleted &= dest.hasCar;
        }
        if (levelCompleted)
        {
            GameManager.instance.LevelDone();
        }
    }
    public void InitLevel(int levelIndex)
    {
        int carCount = 1;
        if (levelIndex < 4)
        {
            carCount = levelIndex + 1;
            Random.InitState(levelIndex);
            for (int i = 0; i < carCount; i++)
            {
                float r = Random.Range(0.2f, 1f) / Random.Range(0.7f, 2f);
                float g = Random.Range(0.2f, 1f) / Random.Range(0.7f, 2f);
                float b = Random.Range(0.2f, 1f) / Random.Range(0.7f, 2f);

                Color color = new Color(r, g, b);
                colors.Add(color);


                float ypos = (i - (carCount - 1f)/2) * 4;
                float xpos = 16;
                Vector3 pos = new Vector3(xpos, 0, ypos) + transform.position;
                CarSpawn carSpawn = Instantiate(prefCarSpawn, pos, transform.rotation, transform).GetComponent<CarSpawn>();
                carSpawns.Add(carSpawn);
                carSpawn.Init(i);
                xpos = -16;
                pos = new Vector3(xpos, 0, ypos) + transform.position;
                CarDestination carDestination = Instantiate(prefCarDestination, pos, transform.rotation, transform).GetComponent<CarDestination>();
                carDestinations.Add(carDestination);
                carDestination.Init(i);

            }
        }
        else
        {
            Random.InitState(levelIndex);
            carCount = Random.Range(1, 5); // 5- exclusive

            int swapD = Random.Range(0, carCount);

            int swapS = Random.Range(0, 4);
            for (int i = 0; i < carCount; i++)
            {
                float r = Random.Range(0.2f, 1f) / Random.Range(0.7f, 2f);
                float g = Random.Range(0.2f, 1f) / Random.Range(0.7f, 2f);
                float b = Random.Range(0.2f, 1f) / Random.Range(0.7f, 2f);

                Color color = new Color(r, g, b);
                colors.Add(color);

                int newi = (i + swapS) % carCount;
                float ypos = (newi - (carCount - 1f) / 2) * 4;
                float xpos = 16;
                Vector3 pos = new Vector3(xpos, 0, ypos) + transform.position;
                CarSpawn carSpawn = Instantiate(prefCarSpawn, pos, transform.rotation, transform).GetComponent<CarSpawn>();
                carSpawns.Add(carSpawn);
                carSpawn.Init(i);
                xpos = -16;
                newi = (i + swapD) % carCount;
                ypos = (newi - (carCount - 1f) / 2) * 4;
                pos = new Vector3(xpos, 0, ypos) + transform.position;
                CarDestination carDestination = Instantiate(prefCarDestination, pos, transform.rotation, transform).GetComponent<CarDestination>();
                carDestinations.Add(carDestination);
                carDestination.Init(i);

            }

        }
    }

    public void MoveLevel(Vector3 position, UnityAction OnDone)
    {
        foreach (CarSpawn spawn in carSpawns)
        {
            if (spawn.car != null)
            {
                spawn.car.GetComponent<Rigidbody>().isKinematic = true;
                spawn.car.GetComponent<Rigidbody>().velocity = new Vector3();
            }
        }
        moveTo = position;
        moveLevel = true;
        onDoneMovementAction = OnDone;
    }
}
